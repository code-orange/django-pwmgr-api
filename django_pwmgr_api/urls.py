from django.urls import path, include
from tastypie.api import Api

from django_pwmgr_api.django_pwmgr_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")

# API GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# API CREDENTIALS
v1_api.register(CredentialResource())
v1_api.register(OtpResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
