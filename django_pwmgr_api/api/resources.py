import pyotp
from tastypie.exceptions import Unauthorized, NotFound
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_pwmgr_passbolt.django_pwmgr_passbolt.func import *
from django_pwmgr_passbolt.django_pwmgr_passbolt.models import Resources
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)


class CredentialResource(Resource):
    class Meta:
        queryset = Resources.objects.all()
        always_return_data = True
        allowed_methods = ["get", "post"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()
        limit = 0
        max_limit = 0

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        if not bundle.request.user.auth_api_key.super_admin:
            raise Unauthorized

        passbolt = passbolt_connect()

        bundle.obj = {
            "password": read_credential(passbolt, kwargs["pk"]),
        }

        return bundle.obj

    def obj_create(self, bundle, **kwargs):
        passbolt = passbolt_connect()

        if bundle.request.user.auth_api_key.super_admin == True:
            # Admin can do what he wants
            pass
        else:
            # Check permission
            if not bundle.data["org_tag"] == bundle.request.user.org_tag:
                raise Unauthorized

        cred_data = dict()
        cred_data["name"] = bundle.data["name"]
        cred_data["uri"] = bundle.data["uri"]
        cred_data["username"] = bundle.data["username"]
        cred_data["description"] = bundle.data["description"]

        if "password_cleartext" in bundle.data:
            cred_data["password_cleartext"] = bundle.data["password_cleartext"]

        # Optional
        if "id" in bundle.data:
            cred_id = str(bundle.data["id"])

            # check if it exists
            try:
                resource_find = Resources.objects.filter(id=cred_id)
            except Resources.DoesNotExist:
                return NotFound

            # TODO: check if destination folder matches!
        else:
            cred_id = None

        folder = bundle.data["folder"]

        # create / update entry
        root_folder = find_root_folder(bundle.data["org_tag"])

        if root_folder is False:
            # create root folder
            create_root_folder(passbolt, bundle.data["org_tag"])
            root_folder = find_root_folder(bundle.data["org_tag"])

        sub_folder = find_sub_folder(root_folder, folder)

        if sub_folder is False:
            # create sub folder
            create_sub_folder(passbolt, root_folder, folder)
            sub_folder = find_sub_folder(root_folder, folder)

        cred_data["folder_parent_id"] = sub_folder.id

        bundle.obj = write_credentials_to_folder(
            passbolt, sub_folder, cred_data, cred_id
        )

        return bundle

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class OtpResource(Resource):
    class Meta:
        queryset = Resources.objects.all()
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()
        limit = 0
        max_limit = 0

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        if not bundle.request.user.auth_api_key.super_admin:
            raise Unauthorized

        passbolt = passbolt_connect()

        otp_seed = read_credential(passbolt, kwargs["pk"])

        totp = pyotp.TOTP(otp_seed)

        bundle.obj = {
            "totp": totp.now(),
        }

        return bundle.obj

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle
